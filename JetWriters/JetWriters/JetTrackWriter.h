#ifndef JET_TRACK_WRITER_H
#define JET_TRACK_WRITER_H

#include "JetWriters/JetConstituentWriterFwd.h"
#include "xAODTracking/TrackParticleFwd.h"
#include "xAODJet/JetFwd.h"

// Standard Library things
#include <string>
#include <vector>
#include <memory>

namespace H5 {
  class Group;
}

class JetConstituentWriterConfig;

class JetTrackWriter
{
public:
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  JetTrackWriter(
    H5::Group& output_file,
    const JetConstituentWriterConfig&);
  ~JetTrackWriter();
  JetTrackWriter(JetTrackWriter&) = delete;
  JetTrackWriter operator=(JetTrackWriter&) = delete;
  JetTrackWriter(JetTrackWriter&&);
  void write(const xAOD::Jet& jet, const JetTrackWriter::Tracks& tracks);
  void flush();
private:
  std::unique_ptr<JetConstituentWriter<xAOD::TrackParticle>> m_writer;
};

#endif
