#ifndef JET_ELEMENT_HH
#define JET_ELEMENT_HH

#include "xAODJet/JetFwd.h"

#include <tuple>

template <typename T>
struct JetElement
{
  const T* constituent;
  const xAOD::Jet* jet;
  JetElement(const T* c, const xAOD::Jet* j) :
    constituent(c),
    jet(j)
    {
    }
};

#endif
